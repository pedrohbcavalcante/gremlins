#ifndef _GREMLINS_H_
#define _GREMLINS_H_
#include <iostream>
#include <cstdlib>

class StoragePool{
public:
	StoragePool();
	virtual ~StoragePool();
	virtual void* Allocate (site_t) = 0;
	virtual void Free (void *) = 0;
};

class SLPool : public StoragePool{
public: 
	struct Header{
		unsigned int mui_Length;
		Header() : mui_Length(0u){/*empty*/};
	};

	struct Block: public Header{
		enum {BlockSize = 16};
		union {
			Block *mp_Next;
			char mc_newRawArea[BlockSize - sizeof(Header)];
		};
		Block():Header(), mp_Next(nullptr){/*empty*/};
		
	};
private:
	unsigned int mui_NumberOfBLocks;
	Block *mp_Pool;
	Block &mr_Sentinel;
public: 
	explicit SLPool(size_t);
	~SLPool();
	void * Allocate(size_t);
	void Free(void *);
};
#endif