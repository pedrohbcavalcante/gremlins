	#include <iostream>
#include "gremilins.h"
typedef long unsigned int size_t;


SLPool::SlPool(size_t bytes){
	if ((bytes % BlockSize) != 0){
		bytes = ((bytes / BlockSize) - 1) * BlockSize;
	}
	bytes = bytes - BlockSize;
	Number_blocks = bytes/BlockSize;

	Block Pool{Number_blocks};
	mp_Pool = Pool;
	mr_sentinel.Header.Length = 0;
	mr_sentinel.next = mp_Pool;
	mp_Pool->Header.Length = Number_blocks;
}

SLPool::~SlPool(){

};
