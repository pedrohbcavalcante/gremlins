#ifndef _MEMPOOL_COMMON_H_
#define _MEMPOOL_COMMON_H_

#include <iostream>
#include <cstdlib>

struct Tag {
	StoragePool * pool;
};

void * operator new(size_t bytes, StoragePool & p){
	Tag* const tag = reinterpret_cast <Tag *> (p.Allocate(bytes + sizeof(Tag)));
	tag->pool = &p;
	return tag + 1U;
}

void * operator new (size_t bytes){
	Tag* const tag = reinterpret_cast <Tag *> (std::malloc(bytes + sizeof(Tag)));
	tag->pool = nullptr;
	return (reinterpret_cast<void *>(tag + 1U));
}

void operator delete (void * arg) noexcept {
	Tag * const tag = reinterpret_cast<Tag *> (arg) - 1U;
	if (nullptr != tag->pool){
		tag->pool->Release (tag);
	}else{
		std::free(tag);
	}
}
#endif